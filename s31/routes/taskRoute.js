//contains all the endpoints for our application
// we separate the routes such that "app.js" only contains information on the server
const express = require("express");

//Create a Router instance that functions as a middleware and routing system
//Allow access to HTTP method middleware that makes it easier to create routes for our application
const router = express.Router();

const taskController = require ("../controllers/taskControllers");
const task = require("../models/task");

// [Routes]
//ROute for getting all the tasks in our db
router.get('/', (req, res)=>{
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})
//Route for creating a task
router.post('/', (req,res)=>{
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

//Route for Deleting a task
router.delete('/:task', (req, res) => {
   
    console.log(req.params.task);
    let taskId = req.params.task;
    //res.send(`HELLO FROM DELETE`);//tempo response
    taskController.deleteTask(taskId).then(resultID => res.send(resultID));
    
});

// Route for Updating a task (status)
 router.put('/:task', (req, res) =>{
     console.log(req.params.task);
     let taskId =req.params.task;
     taskController.taskCompleted(taskId).then(outcome => res.send(outcome));
 });

 //Route again for Updating task status(completed -> pending)..this will be a counter procedure for the previous task above.
 router.put('/:task/pending',(req, res) =>{
     let id = req.params.task;
     //console.log(id);
     taskController.taskPending(id).then(outcome => res.send(outcome));

 });


// Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;
