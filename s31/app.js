//[SECTION]Dependencies and Modules
    const express = require ("express");
    const mongoose = require ("mongoose");
    const taskRoute = require ("./routes/taskRoute");

//[SECTION] Server Setup
    const app = express();
    const port =4000;

    app.use(express.json());
    app.use(express.urlencoded({extended: true}));


//[SECTION] Database Connection/Setup
    mongoose.connect(`mongodb+srv://djrosales:admin123@cluster0.w5fdk.mongodb.net/toDo176?retryWrites=true&w=majority`, {
        useNewUrlParser:true,
        useUnifiedTopology:true
    });
  
    
    let db = mongoose.connection;
    db.on('error',console.error.bind(console,"Connection Error"));
    db.once('open',() => console.log("Connected to MongoDB"));


//[SECTION] Routing System
     //Add the task route
     app.use("/tasks", taskRoute);


//[SECTION]Entry Point Response
    app.listen(port, () => console.log(`Server running at port ${port}`));